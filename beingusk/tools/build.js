({
    appDir: '../www',
    baseUrl: '../www',
    dir: '../www-built',
    mainConfigFile: '../www/config.js',
    name: 'app',
    findNestedDependencies: true,
    modules: [
        //First set up the common build layer.
        

        //Now set up a build layer for each main layer, but exclude
        //the common one. "exclude" will exclude nested
        //the nested, built dependencies from "common". Any
        //"exclude" that includes built modules should be
        //listed before the build layer that wants to exclude it.
        //The "page1" and "page2" modules are **not** the targets of
        //the optimization, because shim config is in play, and
        //shimmed dependencies need to maintain their load order.
        //In this example, common.js will hold jquery, so backbone
        //needs to be delayed from loading until common.js finishes.
        //That loading sequence is controlled in page1.html.
        {
            //module names are relative to baseUrl/paths config
            name: '../www/app'
            
        }
    ]
})
