/**
 * Created by UdaySravanK on 4/24/17.
 */
define([
    'jquery',
    'page',
    'underscore',
    'backbone',
    'js/models/tax.model',
    'router',
    'text!templates/tax.html'
], function ($, Page, _, Backbone, PageModel, Router, Template) {
    var newPage = Page.extend({
        constructor: function JSProgramsPage() {
            Page.prototype.constructor.apply(this, arguments);
        },
        model: new PageModel(),

        templates: {
            pageTpl: Template
        },
        events: {},
        initializeView: function () {
            this.bindBackboneListeners();
        },

        initializePatLab: function () {
        },
        renderView: function () {
            this.compiledTemplate = _.renderPartial(self.templates['pageTpl'], this.model.tplData);
            this.$el.html(this.compiledTemplate);
        }
    }).mixin([]);

    return newPage;
});