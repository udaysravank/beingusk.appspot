/**
 * Created by UdaySravanK on 6/21/17.
 */
define([
    'jquery',
    'page',
    'underscore',
    'backbone',
    'js/models/ds-algo-notes.model',
    'router',
    'text!templates/ds-algo-notes.html'
], function ($, Page, _, Backbone, PageModel, Router, Template) {
    var newPage = Page.extend({
        constructor: function DSAndAlgoNotes() {
            Page.prototype.constructor.apply(this, arguments);
        },
        model: new PageModel(),

        templates: {
            pageTpl: Template
        },
        events: {},
        initializeView: function () {
        },

        initializePatLab: function () {
        },
        renderView: function () {
            var compiledTemplate = _.renderPartial(this.templates['pageTpl'], this.model.tplData);
            this.$el.html(compiledTemplate);
        }
    }).mixin([]);

    return newPage;
});