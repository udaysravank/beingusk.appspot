/**
 * Created by UdaySravanK on 4/24/17.
 */
define([
    'jquery',
    'page',
    'global',
    'underscore',
    'backbone',
    'js/models/lifestyle.model',
    'router',
    'text!templates/lifestyle.html'
], function ($, Page, Global, _, Backbone, LifestyleModel, Router, LifestyleTpl) {
    /**
     * CONSTRUCTOR
     ***************************************************************************/
    var LifestylePage = Page.extend({
        constructor: function LifestylePage() {
            Page.prototype.constructor.apply(this, arguments);
        },

        /**
         * MODEL DATA
         ***********************************************************************/
        model: new LifestyleModel(),

        templates: {
            lifestylePage: LifestyleTpl
        },

        /**
         * EVENTS
         * - add events that are specific to phone here
         * - all common events are in the mixin
         ***********************************************************************/
        events: {},

        /**
         * EXECUTION
         ***********************************************************************/

        /**
         * Initialize View
         * @method initializeView
         ***********************************************************************/
        initializeView: function () {
            this.bindBackboneListeners();
            //this.listenTo(Backbone.Events, 'vi:headerview:popularProduct', this.fetchRecommendedItems);
        },

        /**
         * Initialize PatLab
         * - Add all patlab plugins here
         *
         * @method initializePatLab
         ***********************************************************************/
        initializePatLab: function () {
            //this.patlab.header = new PatLabHeader('[data-behavior=header]', {});
        },

        /**
         * Render View
         * @method renderView
         ***********************************************************************/
        renderView: function () {
            var self = this;

            // compile and render template
            this.compiledTemplate = _.renderPartial(self.templates['lifestylePage'], this.model.tplData);
            this.$el.html(this.compiledTemplate);

            //TODO: initiate css for this view
        }
    }).mixin([]);

    return LifestylePage;
});