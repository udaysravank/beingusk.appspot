/**
 * Created by UdaySravanK on 6/21/17.
 */
define([
    'underscore',
    'backbone',
    'js/controllers/common/base.controller'
], function (_, Backbone, BaseController) {
    var controller = BaseController.extend({
        /**
         * Description
         * @method constructor
         * @return
         */
        constructor: function AndroidNotesController() {
            BaseController.prototype.constructor.apply(this, arguments);
        },

        /**
         * Description
         * This Method will instantiate the header and the footer views
         * @method process
         * @return
         */
        process: function () {

        }
    }).mixin();

    return controller;
});