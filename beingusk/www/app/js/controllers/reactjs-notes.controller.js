/**
 * Created by UdaySravanK on 6/22/17.
 */
define([
    'underscore',
    'backbone',
    'js/controllers/common/base.controller'
], function (_, Backbone, BaseController) {
    var controller = BaseController.extend({
        constructor: function ReactJSNotesController() {
            BaseController.prototype.constructor.apply(this, arguments);
        },
        process: function () {
        }
    }).mixin();

    return controller;
});