/**
 * Created by UdaySravanK on 6/21/17.
 */
define([
    'underscore',
    'backbone',
    'js/controllers/common/base.controller'
], function (_, Backbone, BaseController) {
    var controller = BaseController.extend({
        constructor: function DSandAlogNotesController() {
            BaseController.prototype.constructor.apply(this, arguments);
        },
        process: function () {
        }
    }).mixin();

    return controller;
});