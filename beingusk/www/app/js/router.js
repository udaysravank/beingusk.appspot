/**
 * Created by UdaySravanK on 2/28/16.
 */
/*global define,$*/
define(['backbone', 'lazyLoader'], function (Backbone, Lazyloader) {
    "use strict"
    var Router = Backbone.Router.extend({

        currentController: null,
        constructor: function Router() {
            Backbone.Router.prototype.constructor.apply(this, arguments);
        },
        routes: {
            '': 'home',
            'about': 'about',
            'ios-interview': 'iosInterview',
            'web-interview': 'webInterview',
            'fitness': 'fitness',
            'gallery': 'gallery',
            'tax': 'tax',
            'articles': 'articles',
            'theftcase': 'theftcase',
            'lifestyle': 'lifestyle',
            'js-programs': 'jsPrograms',
            'android-notes': 'android_notes',
            'ds-algo-notes': 'ds_algo_notes',
            'general-notes': 'general_notes',
            'angular-notes': 'angular_notes',
            'reactjs-notes': 'reactjs_notes'
        },
        initialize: function () {
            //var domainUrl = window.location.origin;
            //var basepath = domainUrl.replace(window.location.protocol + "//" + window.location.host + "/", "");
            //this.route(basepath + "/public/index.html", "home");

            //this.on('route:defaultAction', this.home);
            //this.on('route:about', this.about);
        },
        controllerMap: {
            home: "js/controllers/home.controller",
            about: "js/controllers/about.controller",
            iOSInterview: "js/controllers/iOSInterview.controller",
            webInterview: "js/controllers/webInterview.controller",
            fitness: "js/controllers/fitness.controller",
            gallery: "js/controllers/gallery.controller",
            tax: "js/controllers/tax.controller",
            articles: "js/controllers/articles.controller",
            theftcase: "js/controllers/theftcase.controller",
            lifestyle: "js/controllers/lifestyle.controller",
            jsPrograms: "js/controllers/js-programs.controller",
            android_notes: "js/controllers/android-notes.controller",
            ds_algo_notes: "js/controllers/ds-algo-notes.controller",
            general_notes: "js/controllers/general-notes.controller",
            angular_notes: "js/controllers/angular-notes.controller",
            reactjs_notes: "js/controllers/reactjs-notes.controller"
        },
        home: function () {
            Lazyloader.loadController([this.controllerMap.home], this, 'home', this.currentController);
        },
        about: function () {
            Lazyloader.loadController([this.controllerMap.about], this, 'about', this.currentController);
        },
        iosInterview: function () {
            Lazyloader.loadController([this.controllerMap.iOSInterview], this, 'iOSInterview', this.currentController);
        },
        webInterview: function () {
            Lazyloader.loadController([this.controllerMap.webInterview], this, 'webInterview', this.currentController);
        },
        fitness: function () {
            Lazyloader.loadController([this.controllerMap.fitness], this, 'fitness', this.currentController);
        },
        gallery: function () {
            Lazyloader.loadController([this.controllerMap.gallery], this, 'gallery', this.currentController);
        },
        tax: function () {
            Lazyloader.loadController([this.controllerMap.tax], this, 'tax', this.currentController);
        },
        articles: function () {
            Lazyloader.loadController([this.controllerMap.articles], this, 'articles', this.currentController);
        },
        theftcase: function () {
            Lazyloader.loadController([this.controllerMap.theftcase], this, 'theftcase', this.currentController);
        },
        lifestyle: function () {
            Lazyloader.loadController([this.controllerMap.lifestyle], this, 'lifestyle', this.currentController);
        },
        jsPrograms: function () {
            Lazyloader.loadController([this.controllerMap.jsPrograms], this, 'jsPrograms', this.currentController);
        },
        android_notes: function () {
            Lazyloader.loadController([this.controllerMap.android_notes], this, 'android_notes', this.currentController);
        },
        ds_algo_notes: function () {
            Lazyloader.loadController([this.controllerMap.ds_algo_notes], this, 'ds_algo_notes', this.currentController);
        },
        general_notes: function () {
            Lazyloader.loadController([this.controllerMap.general_notes], this, 'general_notes', this.currentController);
        },
        angular_notes: function () {
            Lazyloader.loadController([this.controllerMap.angular_notes], this, 'angular_notes', this.currentController);
        },
        reactjs_notes: function () {
            Lazyloader.loadController([this.controllerMap.reactjs_notes], this, 'reactjs_notes', this.currentController);
        },
        initController: function (controller, context) {
            // reset header
            if (controller !== undefined) {
                if (this.currentController) {
                    this.currentController.stopListening();
                    delete this.currentController;
                }
                this.currentController = controller;

                if (_.isEmpty(this.routeData)) {
                    controller.setPageMetaData(context);
                } else {
                    controller.setPageMetaData(context, _.extend({}, this.routeData));
                    this.routeData = {};
                }

                controller.Shell.setMasterHTML(controller.pageMetaData.page_grid);
                this.listenTo(controller, "navigate", this.navigateInitiate);
                controller.lazLdResource(controller.Shell, controller);

                // Always hide modals
                $('body').removeClass('has-backdrop has-backdrop-fixed');
            }
        }
    });
    return Router;
});
