/**
 * Created by UdaySravanK on 4/24/17.
 */
define([
    'jquery',
    'backbone',
    'js/models/common/base.model'
], function ($, Backbone, BaseModel) {
    var taxModel = BaseModel.extend({
        defaults: {
            welcomeText: 'Hey there!!'
        },
        tplData: {
            title: 'Income tax related information'
        },
        initialize: function () {
        }
    });

    return taxModel;
});