/**
 * Created by UdaySravanK on 4/24/17.
 */
define([
    'jquery',
    'backbone',
    'js/models/common/base.model'
], function ($, Backbone, BaseModel) {
    var theftCaseModel = BaseModel.extend({
        defaults: {
            welcomeText: 'Hey there!!'
        },
        tplData: {
            title: 'theftCaseModel'
        },
        initialize: function () {
        }
    });

    return theftCaseModel;
});