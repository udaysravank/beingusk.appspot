/**
 * Created by UdaySravanK on 6/22/17.
 */
define([
    'jquery',
    'backbone',
    'js/models/common/base.model'
], function ($, Backbone, BaseModel) {
    var model = BaseModel.extend({
        defaults: {
            welcomeText: 'Hey there!!'
        },
        tplData: {
            title: 'Notes and Questions on Angular JS Framework'
        },
        initialize: function () {
        }
    });

    return model;
});