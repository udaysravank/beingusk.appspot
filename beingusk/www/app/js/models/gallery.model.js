/**
 * Created by UdaySravanK on 3/26/17.
 */
define([
    'jquery',
    'backbone',
    'js/models/common/base.model'
], function ($, Backbone, BaseModel) {
    var galleryModel = BaseModel.extend({
        defaults: {
            welcomeText: 'Hey there!!'
        },
        tplData: {
            title: 'gallery page'
        },
        initialize: function () {
        }
    });

    return galleryModel;
});