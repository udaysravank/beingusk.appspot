/**
 * Created by UdaySravanK on 4/24/17.
 */
define([
    'jquery',
    'backbone',
    'js/models/common/base.model'
], function ($, Backbone, BaseModel) {
    var articlesModel = BaseModel.extend({
        defaults: {
            welcomeText: 'Hey there!!'
        },
        tplData: {
            title: 'articlesModel'
        },
        initialize: function () {
        }
    });

    return articlesModel;
});