/**
 * Created by UdaySravanK on 3/26/17.
 */
define([
    'jquery',
    'backbone',
    'js/models/common/base.model'
], function ($, Backbone, BaseModel) {
    var webInterviewModel = BaseModel.extend({
        defaults: {
            welcomeText: 'Hey there!!'
        },
        tplData: {
            title: 'Notes and Questions on Web Technologies'
        },
        initialize: function () {
        }
    });

    return webInterviewModel;
});