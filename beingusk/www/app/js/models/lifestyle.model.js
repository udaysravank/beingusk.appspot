/**
 * Created by UdaySravanK on 4/24/17.
 */
define([
    'jquery',
    'backbone',
    'js/models/common/base.model'
], function ($, Backbone, BaseModel) {
    var LifestyleModel = BaseModel.extend({
        defaults: {
            welcomeText: 'Hey there!!'
        },
        tplData: {
            title: 'LifestyleModel'
        },
        initialize: function () {
        }
    });

    return LifestyleModel;
});