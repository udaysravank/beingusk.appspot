/**
 * Created by UdaySravanK on 3/26/17.
 */
define([
    'jquery',
    'backbone',
    'js/models/common/base.model'
], function ($, Backbone, BaseModel) {
    var iOSInterviewModel = BaseModel.extend({
        defaults: {
            welcomeText: 'Hey there!!'
        },
        tplData: {
            title: 'Notes and Questions on iOS Technologies'
        },
        initialize: function () {
        }
    });

    return iOSInterviewModel;
});