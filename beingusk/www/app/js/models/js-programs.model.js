/**
 * Created by UdaySravanK on 6/21/17.
 */
define([
    'jquery',
    'backbone',
    'js/models/common/base.model'
], function ($, Backbone, BaseModel) {
    var model = BaseModel.extend({
        defaults: {
            welcomeText: 'Hey there!!'
        },
        tplData: {
            title: 'Javascript programing questions and my solutions'
        },
        initialize: function () {
        }
    });

    return model;
});