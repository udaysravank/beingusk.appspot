/**
 * Created by UdaySravanK on 4/25/17.
 */
module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            options: {
                punctuation: ''
            },
            js: {
                files: [
                    {cwd: 'node_modules/bootstrap/dist/css', src: ['*.css'], dest: 'public/libs/bootstrap/css', expand: true},
                    {cwd: 'node_modules/bootstrap/dist/js', src: ['*.js'], dest: 'public/libs/bootstrap/js', expand: true},
                    // {cwd: 'node_modules/bootstrap/fonts', src: ['*.*'], dest: 'public/libs/bootstrap/fonts', expand: true},
                    {cwd: 'node_modules/bootstrap-select/dist/css', src: ['*.css'], dest: 'public/libs/bootstrap-select/css', expand: true},
                    {cwd: 'node_modules/bootstrap-select/dist/js', src: ['*.js'], dest: 'public/libs/bootstrap-select/js', expand: true},
                    {cwd: 'node_modules/bootstrap-validator/dist', src: ['*.js'], dest: 'public/libs/bootstrap-validator/js', expand: true},
                    {cwd: 'node_modules/font-awesome/css', src: ['*.css'], dest: 'public/libs/font-awesome/css', expand: true},
                    // {cwd: 'node_modules/font-awesome/fonts', src: ['Font*.*', 'font*.*'], dest: 'public/libs/font-awesome/fonts', expand: true},
                    {cwd: 'node_modules/jquery/dist', src: ['*.js'], dest: 'public/libs/jquery', expand: true},
                    {cwd: 'node_modules/jquery-ui', src: ['*.js'], dest: 'public/libs/jquery-ui', expand: true},
                    {cwd: 'node_modules/jquery-ui/scripts', src: ['*.js'], dest: 'public/libs/jquery-ui/scripts', expand: true},
                    {cwd: 'node_modules/jquery-ui/themes', src: ['*.js'], dest: 'public/libs/jquery-ui/themes', expand: true},
                    {cwd: 'node_modules/jquery-ui-touch-punch', src: ['*.js'], dest: 'public/libs/jquery-ui-touch-punch', expand: true},
                    {cwd: 'node_modules/jquery.cookie', src: ['*.js'], dest: 'public/libs/jquery.cookie', expand: true},
                    {cwd: 'node_modules/backbone', src: ['*.js'], dest: 'public/libs/backbone', expand: true},
                    {cwd: 'node_modules/backbone/node_modules/underscore', src: ['*.js'], dest: 'public/libs/underscore', expand: true},
                    {cwd: 'node_modules/underscore.string/dist', src: ['*.js'], dest: 'public/libs/underscore', expand: true}
                ]
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['www/**/*.js'],
                dest: 'dist/<%= pkg.name %>.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },
        jshint: {
            files: ['Gruntfile.js', 'www/**/*.js'],
            options: {
                // options here to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },
        watch: {
            files: ['<%= jshint.files %>']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('default', ['concat', 'uglify']);

};